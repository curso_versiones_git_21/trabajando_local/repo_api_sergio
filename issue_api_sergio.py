import requests
id_project = "curso_versiones_git_21%2Ftrabajando_local%2Frepo_api_sergio"
#id_project = "curso_versionesgit2%trabajando_loca%repo_grupal"
url = f"https://gitlab.com/api/v4/projects/{id_project}/issues"
headers = {"PRIVATE-TOKEN": "glpat-JvCm3mFFc7xWcdXM5XtJ"}
data = {"title": "issue_api_sergio"}

response = requests.post(url, headers=headers, data=data)

if response.status_code == 201:
    new_issue = response.json()
    print(f"Nuevo Issue ID: {new_issue['id']}, Title: {new_issue['title']}")
else:
    print(f"Error: {response.status_code}, {response.text}")